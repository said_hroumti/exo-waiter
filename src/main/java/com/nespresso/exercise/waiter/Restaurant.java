package com.nespresso.exercise.waiter;

import java.security.InvalidParameterException;
import java.util.HashMap;
import java.util.Map;

public class Restaurant {

	private final static String ORDER_SEPARATOR = ": ";
	private Map<Integer, Table> tables = new HashMap<Integer, Table>();
	private Order order;

	public Restaurant() {
		order = new Order();
	}

	public int initTable(int sizeOfTable) {
		Table table = new Table();
		table.init(tables.size(), sizeOfTable);
		tables.put(table.getId(), table);
		return table.getId();
	}

	public void customerSays(int tableId, String message) {
		Table table = tables.get(tableId);
		if (table == null) {
			throw new InvalidParameterException("The tabe " + tableId + " does not exist");
		}

		Customer customer = new Customer(message.split(ORDER_SEPARATOR)[0]);
		customer.order(table, message.split(ORDER_SEPARATOR)[1]);

	}

	public String createOrder(int tableId) {
		return order.createOrder(tables.get(tableId));
	}
}
