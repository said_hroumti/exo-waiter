package com.nespresso.exercise.waiter;

public class Order {

	private static final String CREATE_ORDER_SEPARATOR = ", ";
	private static final String SAME_AS_PREVIEWS_ITEM = "Same";

	public String createOrder(Table table) {
		int missingOrders = table.getMissingOrders();
		if (missingOrders > 0) {
			return "MISSING " + missingOrders;
		}

		StringBuilder result = new StringBuilder();
		int index = 0;
		String previewsOrderItem = null;
		for (Customer customer : table.getCustomers()) {
			if (SAME_AS_PREVIEWS_ITEM.equalsIgnoreCase(customer.getOrder()) && previewsOrderItem != null) {
				result.append(previewsOrderItem);
			} else {
				result.append(customer.getOrder());
				previewsOrderItem = customer.getOrder();
			}
			if (index < table.getCustomers().size() - 1) {
				result.append(CREATE_ORDER_SEPARATOR);
			}
			index++;
		}

		return result.toString();

	}

}
