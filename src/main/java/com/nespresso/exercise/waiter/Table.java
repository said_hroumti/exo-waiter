package com.nespresso.exercise.waiter;

import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

public class Table {

	private int id;
	private int size;
	private Map<String, Customer> customers = new LinkedHashMap<String, Customer>();

	public int getId() {
		return id;
	}

	public void addCustomer(Customer customer) {
		if (customers.size() == size && !customers.containsKey(customer.getName())) {
			throw new UnsupportedOperationException("Not enough places in the table.");
		}
		customers.put(customer.getName(), customer);
	}

	public void init(int id, int size) {
		this.id = id;
		this.size = size;
	}

	public int getMissingOrders() {
		Set<String> itemsForTwo = new HashSet<String>();
//		for (Customer customer : customers.values()) {
//			// TODO to generalize
//			if(customer.getOrder().endsWith("for 2")){
//				if(itemsForTwo.contains(customer.getOrder().replace("for 2", ""))){
//					
//				}else
//			}
//		}
		return size - customers.size();
	}

	public Collection<Customer> getCustomers() {
		return customers.values();
	}

}
