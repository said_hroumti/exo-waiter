package com.nespresso.exercise.waiter;

public class Customer {

	private String name;
	private String order;

	public Customer(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void order(Table table, String command) {
		this.order = command;
		table.addCustomer(this);
	}

	public String getOrder() {
		return order;
	}

}
